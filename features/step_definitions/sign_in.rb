Given(/^I am not logged in visitor$/) do
  # visit 'http://192.168.56.102/'
  @home_page = HomePage.new
  @home_page.load
end

When(/^I click sign in button$/) do
  # find(:xpath, '//*[@id="account"]/ul/li[1]/a').click
  @home_page.top_menu.sign_in_link.click
end

Then(/^I see the log in form is opened$/) do
  expect(current_url).to include '/login'
  expect(page).to have_content 'Login'
  expect(page).to have_content 'Password'
end

When(/^I fill in log in for with valid credentials$/) do
  @login_page = LoginPage.new

  @login_page.user_name_filed.set 'user'
  @login_page.password_filed.set '8ktcbP4cxpWV'
  # find('#username').set 'user'
  # find('#password').set '8ktcbP4cxpWV'
end

And(/^I click log in button$/) do
  @login_page.submit_btn.click
  # find('#login-submit').click
end

Then(/^I become a logged in as "([^"]*)"$/) do |user_name|
  expect(page).to have_content "Logged in as #{user_name}"
end

Given(/^I am logged in as "([^"]*)"$/) do |user_name|
  login user_name, 'qwerty1234'
end