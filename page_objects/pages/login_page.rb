class LoginPage < SitePrism::Page
  set_url 'http://192.168.56.102/login'

  element :user_name_filed, '#username'
  element :password_filed, '#password'
  element :submit_btn, '#login-submit'
end